using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class is for the enemy detection sphere and allows it to grow.
public class Enemy: MonoBehaviour
{
    private Rigidbody rb;
    public GameObject redcube;
    private bool PlayerEntered = false;
    private bool maxsizereached = false;
    private float vectormax = (float)0.5;

    //On start get rigid body
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update function to allow the cube to grow if size is not max
    void FixedUpdate()
    {
        if ((PlayerEntered == true) && (maxsizereached == false))
        {
            if (redcube.transform.localScale.x <= vectormax)
            {
                redcube.transform.localScale += new Vector3((float)0.01, 0, (float)0.01);
            }
       
        }
    }
    //Detects player collision and sets bool to true
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            PlayerEntered = true;
        }
    }


}