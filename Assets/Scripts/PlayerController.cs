using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections;
using UnityEngine.UI;
using TMPro;

//This class is for the player object and controller
public class PlayerController : MonoBehaviour
{
	//Variables for health, speed, score, gametime, rigidbody, UItexts
	public int health = 5;
	private int count;
	public TextMeshProUGUI countText;
	public TextMeshProUGUI healthText;
	public TextMeshProUGUI dashText;
	public TextMeshProUGUI gameTimeText;
	public TextMeshProUGUI winTextObject;
	public TextMeshProUGUI loseTextObject;
	public AudioSource PickupSound;
	private float movementX;
	private float movementY;
	public float speed = 10;
	private Rigidbody rb;
	private bool dashing = false;
	private float gameTime = 0;

	//On start set default values and get rigidbody
	void Start()
	{
		rb = GetComponent<Rigidbody>();
		count = 0;
		SetCountText();
		SetHealthText();
		winTextObject.gameObject.SetActive(false);
		loseTextObject.gameObject.SetActive(false);
		dashText.text = "Dash: Ready";
	}

	//Every x updates update player location through speed
	void FixedUpdate()
	{
		Vector3 movement = new Vector3(movementX, 0.0f, movementY);
		rb.AddForce(movement * speed);
	}

	//every update, update the gametime and redisplay timer
	void Update()
    {
		gameTime += Time.deltaTime;
		int seconds = (int)gameTime;
		gameTimeText.text = "Time:" + seconds.ToString();

	}

	//On collision check if it was a pickup or enemy
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);
			count = count + 1;
			SetCountText();
			PickupSound.Play();
		}
		else if (other.gameObject.CompareTag("Enemy"))
		{
			health = health - 1;
			SetHealthText();
		}
	}

	//onmovement update x and y vectors
	void OnMove(InputValue value)
	{
		Vector2 v = value.Get<Vector2>();
		movementX = v.x;
		movementY = v.y;
	}

	//Sets count to current value and checks if player has obtained all pickups
	void SetCountText()
	{
		countText.text = "Count: " + count.ToString();
		int seconds = (int)gameTime;
		if (count >= 12)
		{
			if(!loseTextObject.gameObject.activeInHierarchy)
            {
				winTextObject.gameObject.SetActive(true);
				winTextObject.text = "You win! Time: " + seconds.ToString();
			}
			
		}
	}

	//Sets health stat to current value and checks if player has died and lost.
	void SetHealthText()
	{
		healthText.text = "HP: " + health.ToString();

		if (health <= 0)
		{
			int seconds = (int)gameTime;
			loseTextObject.gameObject.SetActive(true);
			loseTextObject.text = "You Lose! Time: " + seconds.ToString();
			winTextObject.gameObject.SetActive(false);
			speed = 0;
		}
	}

	//Enables dashing at high speeds. Checks if dash is available and then uses two coroutines as timers.
	void OnDash()
    {
		float DashCooldown = 10;
		float DashingTime = 5;
		if(dashing.Equals(false))
        {
			dashText.text = "Dash: Unready";
			speed = 30;
			dashing = true;
			StartCoroutine(DuringDashTimer(DashingTime));
			StartCoroutine(DashWaitTimer(DashCooldown));
		}
    }
	//Timer 1 for dash movement bonus
	IEnumerator DuringDashTimer(float timetowait)
    {
		Debug.Log("Dash Start: " + Time.time);
		yield return new WaitForSeconds(timetowait);
		speed = 10;
		Debug.Log("Dash End: " + Time.time);
	}
	//timer 2 for dash cooldown
	IEnumerator DashWaitTimer(float timetowait)
	{
		Debug.Log("Wait Start: " + Time.time);
		yield return new WaitForSeconds(timetowait);
		dashing = false;
		Debug.Log("Wait End: " + Time.time);
		dashText.text = "Dash: Ready";
	}

}
